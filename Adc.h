// -*- mode: C++ -*-


namespace DcscAdc 
{
  
  class Adc 
  {
  public:
    enum { 
      com    = 0,
      status = 0,
      mode,
      ctrl,
      filter,
      data,
      offset,
      gain,
      ioctrl,
      id     = 15, 
      reset  = 66
    };
    enum { // status flags
      lock  = (1 << 0),
      err   = (1 << 3), 
      cal   = (1 << 5), 
      ready = (1 << 7)
    };
    enum { // Com flags
      read  = (1 << 6),
      write = (1 << 7)
    };
    enum { // Ctrl flags
      uni_polar = (1 << 3)
    };
    enum { // Mode flags 
      oscilator_pedestal = (1 << 3),
      channel_convert    = (1 << 4),
      reference_select   = (1 << 5),
      negative           = (1 << 6),
      chop               = (1 << 7),
    };
    enum { // States 
      idle             = 1,
      single           = 2,
      continous        = 3,
      calibrate_zero   = 4, 
      calibrate_full   = 5, 
      md               = 7
    };
      
      
      

    Adc(const char* device="/dev/adc");
    
    int  Setup();

    int  Read(int reg, unsigned char&  value);
    int  Read(int reg, unsigned short& value);
    int  Write(int reg, unsigned char&  value);
    int  Write(int reg, unsigned short& value);

    int  PollReady();

    int  CalibrateZero();
    int  CalibrateFull();
    int  CalibrateAll();

    int  Temperature(float& t, bool fast=true);
    
    void Reset();
  protected:
      
    /** File descriptor */
    int _fd; 
  };
}
