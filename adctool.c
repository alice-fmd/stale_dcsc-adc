#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <sys/time.h>
#include <linux/types.h>
#include <errno.h>
#include <sys/poll.h> 
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include "adc.h"

/* 
 * A PT100 with current sense resistor connected
 * to channel AIN2 and AIN3
 * 
 */
//#define TKSPT100

int debug=0;

/********************************************************************
 * adc_reset Resets adcs 
 *
 * @param fd File descriptor pointing to device file /dev/adc
 */
static void adc_reset(int fd){
  u8 buf[3]={adc_rst,0,0};
  write(fd,buf,3);
  if (debug) printf("reset\n");
}
/********************************************************************
 * adc_read read an ADC register. There are 8 Bit (count=1) and 
 * 16 Bit (count=2) Registers.
 *
 * @param fd File descriptor pointing to device file /dev/adc
 * @param reg Register Address
 * @param value where the read value shoul be stored
 * @param count 1=8 Bit register 2=16 Bit register
 */
static int adc_read(int fd, int reg, u32 *value, int count ) {
  u8 buf[3]={0,0,0};
  int i;

  if ( (count < 1) || (count > 2) ) {
    printf("only 1 (8 Bit) or 2 (16 Bit) reads possible\n");
    return -1;
  }
  buf[0]=(u8) (reg & 0xf);
  i=read(fd,buf,count+1);
  if (count==2) 
    *value = (u32) ((buf[1] | (buf[2]<<8)) & 0xffff);
  else
    *value = (u32) (buf[1] & 0xff);
  if (debug) printf("Read 0x%04X (buf: 0x%02X 0x%02X 0x%02X)\n",*value,buf[0],buf[1],buf[2]);
  return 0;
}
/********************************************************************
 * adc_write write an ADC register. There are 8 Bit (count=1) and 
 * 16 Bit (count=2) Registers. 
 *
 * @param fd File descriptor pointing to device file /dev/adc
 * @param reg Register Address
 * @param value to be written
 * @param count 1=8 Bit register 2=16 Bit register
 */
static int adc_write(int fd, int reg, u32 value, int count ) {
  u8 buf[3]={0,0,0};
  int i;

  if ( (count < 1) || (count >2) ) {
    printf("only 1 (8 Bit) or 2 (16 Bit) reads possible\n");
    return -1;
  }
  buf[0]=(u8) (reg & 0xf);
  buf[1]=(u8) (value & 0xff);
  buf[2]=(u8) ((value & 0xff00) >> 8);
  i=write(fd,buf,count+1);
  if (debug) printf("Wrote 0x%04X (buf: 0x%02X 0x%02X 0x%02X)\n",value,buf[0],buf[1],buf[2]);
  return 0;
}
/*******************************************************************
 * poll_RDY polls the Ready bit in the Control Register of ADC.
 * if Bit is set the A/D Conversion is done
 *
 * @param fd File descriptor pointing to device file /dev/adc
 */
static int poll_RDY(int fd) {
  int s;
  int i=0;
  while (i<100000) {
    adc_read(fd, adc_status,&s,1);
    if ( OC_ISSET(s,adc_status_RDY) ) {
      //      printf("%s: polled %d cycles\n",__FUNCTION__,i); // ~4520 cycles tested
      //            printf("%d ",i); // ~4520 cycles tested
      return 0;
    }
    i++;
  }
  printf("%s: Timeout waiting for RDY\n",__FUNCTION__);
  return -1;
}
/********************************************************************
 * print_gain_offset for debug and calibration purpose
 * Theses are writeable, but better to change with self-calibration
 * see calibration modes in data sheet
 *
 * @param fd File descriptor pointing to device file /dev/adc
 */
static void print_gain_offset(int fd) {
  int i;
  adc_read(fd, adc_gain,&i,2);
  printf("gain  : 0x%04X  ",i);
  adc_read(fd, adc_offset,&i,2);
  printf("offset: 0x%04X\n",i);
}
/********************************************************************
 * calibrate_zero calibrates zero Volt with __internal__ pull to GND
 *
 * @param fd File descriptor pointing to device file /dev/adc
 */
static void calibrate_zero(int fd) {
  int i=0;
  u32 readback;
  printf("Calibrating internal zero");
  adc_write(fd, adc_mode,adc_mode_ZERO | adc_mode_CHCON ,1);
  while (i<ADC_POLLMAX) {
    adc_read(fd,adc_mode,&readback,1);
    if ( (readback & 7) == adc_mode_IDLE) i=101;
    else { printf("."); fflush(stdout); usleep(10000); }
    i++;
  }
  printf("\n");
}
/*******************************************************************
 * calibrate_full calibrates full scale with __internal__ connected 
 * to Vref
 *
 * @param fd File descriptor pointing to device file /dev/adc
 */
static void calibrate_full(int fd) {
  int i=0;
  u32 readback;
  printf("Calibrating internal full");
  adc_write(fd, adc_mode,adc_mode_FULL | adc_mode_CHCON ,1);
  while (i<ADC_POLLMAX) {
    adc_read(fd, adc_mode,&readback,1);
    if ( (readback & 7) == adc_mode_IDLE) i=101;
    else { printf("."); fflush(stdout); usleep(10000);}
    i++;
  }
  printf("\n");
}
/********************************************************************
 * calibrate_all calibrate all channels zero, full (internal)
 * 
 * @param fd File descriptor pointing to device file /dev/adc
 */
static void calibrate_all(int fd) {
  int i=0;
  for (i=0;i<ADCCHANO;i++) {
    printf("Calibration Channel #%d\n",i+1);
    adc_write(fd, adc_ctrl,((addconLUT[i]<<4) | adc_ctrl_UBn | PGA[i] ),1);
    poll_RDY(fd);
    calibrate_zero(fd);
    calibrate_full(fd);
  }
}

/********************************************************************
 * adc_setup Set global (not per channel) parameters according 
 * to flow-chart fig. 16 on p. 32 in datasheet. Also connversion
 * rate and accuracy.
 *
 * @param fd File descriptor pointing to device file /dev/adc
 */
static void adc_setup(int fd) {
  int i;
  int ioctrl_value=0x00;
    // int filter_value=0x45; // default (slow)
  int filter_value=0x00;
  // 1. reset

  // 2. Iocon
  adc_write(fd,adc_ioctrl,ioctrl_value,1);
  // 3. filter
  adc_write(fd,adc_filter,filter_value,1);
  // 4. ADC Control - necessary?
  adc_write(fd,adc_ctrl,adc_ctrl_UBn,1);
  // 5. Mode register a) calibrate b) mode of op
  //  calibrate_zero();
  //  print_gain_offset();
  //  calibrate_full();

  //  adc_write(fd, adc_mode,adc_mode_CONT | adc_mode_CHCON | adc_mode_CHOPn ,1); // 10 channels, no CHOP
  adc_write(fd, adc_mode,adc_mode_CONT | adc_mode_CHCON  ,1); // 10 channels, CHOP
  poll_RDY(fd);
  // set PGA for Channels
  for (i=0;i<ADCCHANO;i++)
    adc_write(fd, adc_ctrl,((addconLUT[i]<<4) | adc_ctrl_UBn | PGA[i] ),1);
  
}

/********************************************************************/
#ifdef TKSPT100
static void adc_temp(int fd){
  int d,i;
  float u[2];
  float r=0,t=0;

  while(1) {
    for (i=1;i<3;i++) {
      adc_write(fd,adc_ctrl,((addconLUT[i]<<4) | adc_ctrl_UBn | PGA[i] ),1);
      poll_RDY(fd);
      adc_read(fd,adc_data,&d,2);
      u[i-1]=(float) ((float)d*(float)(2.5)*(float)(1.024) / ((float) (1<<16) * (float) (1 << (7-PGA[i]) )));
    }
    r = (10000/101*(u[0]/u[1] - 1));
    t = (r/100 - 1)/0.00385;
    printf("U1=%1.9f V / U2=%1.9f V / R=%3.3f Ohm / T=%3.1f �C            \r", u[0],u[1],r,t);
  }
  
}
/********************************************************************/
static void adc_temp_fast(int fd){
  int d,i;
  float v[2];
  float t;

  while(1) {
    for (i=1;i<3;i++) {
      adc_write(fd,adc_ctrl,((addconLUT[i]<<4) | adc_ctrl_UBn | PGA[i] ),1);
      poll_RDY(fd);
      adc_read(fd,adc_data,&d,2);
      v[i-1]=(float) d;
    }
    t= (((200.0 * v[0]/ v[1])) -  201.0) / 0.38885;
    printf("T=%3.1f �C                                 \r", t);
  }
}  
#endif
/********************************************************************/
int main(int argc, char** argv) {
  //int i;
  int temperature=0;
  int repeat=0;
  int reportinfo=0;
  int fd;
  int d,s,m,c;
  float u,u1=0,u2=0;

  char adcdev[]="/dev/adc";
  char *argument;

  if (argc==1) reportinfo=1;

  if (argc==2) {
    argument=argv[1];
    switch (argument[0]) {
    case 'd':
      debug=1;
      break;
    case 'c':
      repeat=1;
      break;
    case 't':
      temperature=1;
      break;
    default:
      reportinfo=1;
      break;
    }
  }
  if (reportinfo) {
    printf("%s - Tool for AD7708 on the DCS Board\n",argv[0]);
    printf("%s c - continous read\n",argv[0]);
    printf("%s d - debug  output\n",argv[0]);
#ifdef TKSPT100
    printf("%s t - read temperature (with circuit connected to AIN2 and AIN3)\n",argv[0]);
#endif
    return -1;
  }
  
  fd = open (adcdev, O_RDWR);
  if(fd < 0) {
    perror(adcdev);
    return -1;
  }
  
  adc_reset(fd);
  //  calibrate_all(fd);
  //  adc_setup(); 
  adc_setup(fd);


  //  print_gain_offset();

  ///  adc_read(adc_filter,&i,1);
  //  printf("filter: 0x%04X\n",i);

  //  adc_read(adc_mode,&i,1);
  //  printf("mode  : 0x%02X\n",i);

  //  adc_read(adc_status,&i,1);
  //  printf("status: 0x%02X\n",i);
#ifdef TKSPT100
  if (temperature) adc_temp_fast(fd);
  if (temperature) adc_temp(fd);
#endif
  do {
    int i;

    for (i=0;i<10;i++) {
      adc_write(fd,adc_ctrl,((addconLUT[i]<<4) | adc_ctrl_UBn | PGA[i] ),1);
      poll_RDY(fd);
      adc_read(fd,adc_mode,&m,1);
      adc_read(fd,adc_status,&s,1);
      adc_read(fd,adc_ctrl,&c,1);
      adc_read(fd,adc_data,&d,2);
      u=(float) ((float)d*(float)(2.5)*(float)(1.024) / ((float) (1<<16) * (float) (1 << (7-PGA[i]) )));
      if (i==1) u1=u;
      if (i==2) u2=u;
      printf("status AIN%02d(%1XF): 0x%02X mode: 0x%02X ADDCON: 0x%02X data: %05d - U: %1.9fV / %1.3fV  \n",
	     i+1,
	     addconLUT[i],
	     s,
	     m,
	     c,
	     d,
	     u,
	     rangeLUT[ PGA[i] ]
	     );
      //      fflush(stdout);
    }
    printf("\n");
    //      usleep(100000);
#ifdef TKSPT100
    {
    float r=0,t=0;
    r = (10000/101*(u1/u2 - 1));
    t = (r/100 - 1)/0.00385;
    printf("R = %fOhm  -  T = %3.1f �C\n", r,t);
    }
#endif
    if (repeat) sleep(1);

  } while (repeat);



  close(fd);
  
  return 0;
}
