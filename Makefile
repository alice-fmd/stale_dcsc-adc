##########################################
# ADC Module and tool
##########################################
include ../config.mk

SRC = adc

kobjects = adc.o 
bobjects = adctool

all : $(kobjects) $(bobjects)

adc.o : adc.c adc.h
	$(CROSSCC) $(KCFLAGS) -c -o $@ $< 

adctool : adctool.c adc.h
	$(CROSSCC) $(CFLAGS) -o $@ $< 

install: all
	cp $(kobjects) $(IMODULEPATH)
	cp $(bobjects) $(IBINPATH)

uninstall: clean
	( cd $(IMODULEPATH) ; rm -f $(kobjects) )
	( cd $(IBINPATH) ; rm -f $(bobjects) )

clean: 
	rm -f $(kobjects) 
	rm -f $(bobjects)

distclean: clean uninstall
	rm -f ./*~


archiv : adc.c adc.h adctool.c 
	echo `date "+$(SRC)_%Y%m%d.tar.bz2"`
	tar cjf `date "+$(SRC)_%Y%m%d.tar.bz2"` -C .. $(SRC)/adc.c $(SRC)/adc.h $(SRC)/adctool.c $(SRC)/Makefile 

