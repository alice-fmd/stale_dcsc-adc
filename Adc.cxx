#include "Adc.h"

namespace {
  struct sys_exception : std::exception
  {
    sys_exception(const char* what) 
      : _msg(what)
    {
      _msg += ": ";
      _msg += strerror(errno);
    }
    ~sys_exception() throws () {}
    const char* what() const throws () { return _msg.c_str(); }
  };
}

//____________________________________________________________________
DcscAdc::Adc::Adc(const char* device)
  : _fd(-1)
{

  _fd = open(device, O_RDWR);
  
  if (fd < 0) 
    throw sys_exception("open");
  
  Reset();
  Setup();
}
//____________________________________________________________________
DcscAdc::Adc::~Adc()
{
  if (_fd >= 0) close(_fd);
  _fd = -1;
}

//____________________________________________________________________
int
DcscAdc::Adc::Reset()
{
  return Write(reset, 0);
}

//____________________________________________________________________
int
DcscAdc::Setup()
{
  _address[0] = 0;
  _address[1] = 1;
  _address[2] = 2;
  _address[3] = 3;
  _address[4] = 4;
  _address[5] = 5;
  _address[6] = 6;
  _address[7] = 7;
  _address[8] = 14;
  _address[9] = 15;

  unsigned char v_ioctrl = 0;
  unsigned char v_filter = 0;
  unsigned char v_mode   = (continous | channel_convert);

  int ret = 0;
  if ((ret = Write(ioctrl, v_ioctrl)) < 0) return ret;
  if ((ret = Write(filter, v_filter)) < 0) return ret;
  if ((ret = Write(mode,   v_mode))   < 0) return ret;

  PollReady();

  for (int i = 0; i < _address.size(); i++) 
    if ((ret = Wrote(ctrl, ((_address[i] << 4) | unipolar | 7)))) return ret;
}

//____________________________________________________________________
int
DcscAdc::Adc::Read(int reg, unsigned char& value)
{
  unsigned char buf[3] = { 0, 0, 0 };
  buf[0] = reg & 0xf;
  int ret = read(_fd, buf, 2);
  value   = buf[1] & 0xff;
  
  return ret;
}
//____________________________________________________________________
int
DcscAdc::Adc::Read(int reg, unsigned short& value)
{
  unsigned char buf[3] = { 0, 0, 0 };
  buf[0] = reg & 0xf;
  int ret = read(_fd, buf, 3);
  value   = (buf[1] | (buf[2] << 8)) & 0xffff;
  
  return ret;
}
//____________________________________________________________________
int
DcscAdc::Adc::Write(int reg, unsigned char value)
{
  unsigned char buf[3] = { 0, 0, 0 };
  buf[0] = reg & 0xf;
  buf[1] = (value & 0xff);
  buf[2] = 0;

  int ret = write(_fd, buf, 2);
  return ret;
}
//____________________________________________________________________
int
DcscAdc::Adc::Write(int reg, unsigned short value)
{
  unsigned char buf[3] = { 0, 0, 0 };
  buf[0] = reg & 0xf;
  buf[1] = (value >> 0) & 0xff;
  buf[2] = (value >> 8) & 0xff;

  int ret = write(_fd, buf, 3);
  return ret;
}
//____________________________________________________________________
int 
DcscAdc::Adc::PollReady()
{
  int i = 0;
  while (i < 10000) { 
    unsigned char s;
    Read(status, s);
    if (s & ready) return 0;
    
    i++;
  }
  return -1;
}
//____________________________________________________________________
int
DcscAdc::Adc::CalibrateZero()
{
  int ret = 0;
  if ((ret = Write(mode, calibrate_zero | channel_convert)) < 0) return ret;
  int i   = 0;
  while (i < 100) {
    unsigned char r;
    if ((ret = Read(mode, r)) < 0) return ret;
    if ((r & 7) == idle)           i = 101;
    else                           usleep(10000);
    i++;
  }
}

//____________________________________________________________________
int
DcscAdc::Adc::CalibrateFull()
{
  int ret = 0;
  if ((ret = Write(mode, calibrate_full | channel_convert)) < 0) return ret;
  int i   = 0;
  while (i < 100) {
    unsigned char r;
    if ((ret = Read(mode, r)) < 0) return ret;
    if ((r & 7) == idle)           i = 101;
    else                           usleep(10000);
    i++;
  }
  return 0;
}
//____________________________________________________________________
int
DcscAdc::Adc::CalibrateAll()
{
  int ret = 0;
  for (int i = 0; i < _address.size(); i++) { 
    unsigned char v = ((_address[i] << 4) | unipolar | 7);
    if ((ret = Write(crl, v)) < 0) return ret;
    
    PollReady();
    
    CalibrateZero();
    CalibrateFull();
  }
  return ret;
}

//____________________________________________________________________
int
DcscAdc::Adc::Temperature(float& t, bool fast)
{
  int ret = 0;
  
  float u[2] = { 0., 0. };

  for (size_t i = 0; i < 2; i++) { 
    unsigned char data = ((_address[i+1] << 4) | unipolar | 7);
    unsigned short v;

    if ((ret = Write(ctrl, data)) < 0) return ret;
    if ((ret = PollReady()) < 0) return ret;
    if ((ret = Read(data, v)) < 0) return ret;
    
    if (fast) 
      u[i] = v;
    else 
      u[i] = v * 2.5 * 1.024 / ((1 << 16) * (1 << (7 - 7)));
  }
  
  if (fast) 
    t       = (((200 * u[0] / u[1])) - 201.) / 0.38885;
  else {
    float r = (10000. / 101 * (u[0] / u[1] - 1));
    t       = (r / 100 - 1) / 0.00385;
  }
  return ret;
}
      
    
