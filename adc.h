#ifndef __KERNEL__
#ifndef u8
typedef unsigned char u8;
#endif

#ifndef u32
typedef unsigned int u32;
#endif


// Number of ADC Channels

#define ADCCHANO 10 

// Lookup Table for Address of channel 0...9

const int addconLUT[10]={0,1,2,3,4,5,6,7,14,15};

// Lookup Table for Voltage Range of channel 

const float rangeLUT[8]={0.020,0.040,0.080,0.160,0.320,0.640,1.280,2.560}; 

// Programmed Gain per channel (0 = 0.020 V ; 7 = 2.56 V)

//const int PGA[10] = {7,4,3,7,7,7,7,7,7,7}; 
const int PGA[10] = {7,7,7,7,7,7,7,7,7,7};

// Device File

#define ADCDEV "/dev/adc"

#define ADC_POLLMAX 100

#endif // __KERNEL
// ADC register
#define adc_com     0
#define adc_status  0
#define adc_mode    1
#define adc_ctrl    2
#define adc_filter  3
#define adc_data    4
#define adc_offset  5
#define adc_gain    6
#define adc_ioctrl  7
#define adc_id      15
#define adc_rst   66

// ADC control register (RW) ADDCON
// unipolar/bipolar

#define adc_ctrl_UBn (1 << 3)   

// ADC communication register (WO)

#define adc_com_WEN (1<<7)
#define adc_com_RWn (1<<6)

// ADC status register (RO)

#define adc_status_RDY  (1 << 7)
#define adc_status_CAL  (1 << 5)
#define adc_status_ERR  (1 << 3)
#define adc_status_LOCK (1 << 0)

// ADC mode register (RW)

#define adc_mode_CHOPn     (1 << 7)
#define adc_mode_NEGBUF    (1 << 6)
#define adc_mode_REFSEL    (1 << 5)
#define adc_mode_CHCON     (1 << 4)
#define adc_mode_OSCPD     (1 << 3)
#define adc_mode_MD        7

#define adc_mode_IDLE      1
#define adc_mode_ZERO      4
#define adc_mode_FULL      5
#define adc_mode_SING      2
#define adc_mode_CONT      3

#define OC_ISSET(reg,bitmask)       ((reg)&(bitmask))
#define OC_ISCLEAR(reg,bitmask)     (!(OC_ISSET(reg,bitmask)))

