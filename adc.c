///////////////////////////////////////////////////////////////////////////////////
//
// device driver for Altera SPI Interface to AD7708 on DCS Board
// 2004 Tobias Krawutschke
//
//

#ifndef __KERNEL__
#define __KERNEL__
#endif
#ifndef MODULE
#define MODULE
#endif

#include <linux/fs.h>
#include <linux/types.h>
#include <linux/sched.h>
#include <linux/errno.h>
#include <linux/slab.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include <linux/poll.h>
#include <linux/mm.h>
#include <linux/ioport.h>
#include <linux/delay.h>
#include <asm/system.h>
#include <linux/module.h>
#include "adc.h"

// base addresses and interrupt request numbers for the devices
// copied out of quartus project \ARM_Stripe_sdk\inc\excalibur.h
// do not mix up with kernel include/asm/arch/excalibur.h !!!
#define na_spi_to_adc                         ((void *) 0x80000040) // altera_avalon_spi
#define na_spi_to_adc_base                              0x80000040
#define na_spi_to_adc_end                     ((void *) 0x80000060)
#define na_spi_to_adc_size                              0x00000020

#define spirxdata      0  // Read-only, 1-16 bit
#define spitxdata      1  // Write-only, same width as rxdata
#define spistatus      2  // Read-only, 9-bit
#define spicontrol     3  // Read/Write, 9-bit
#define spireserved    4  // reserved
#define spislaveselect 5  // Read/Write, 1-16 bit, master only
#define spiendofpacket 6  // Read/write, same width as txdata, rxdata.


// SPI Status Register Bits
#define eop_bit   (1 << 9)
#define	e_bit     (1 << 8)
#define	rrdy_bit  (1 << 7)
#define	trdy_bit  (1 << 6)
#define	tmt_bit   (1 << 5)
#define	toe_bit   (1 << 4)
#define	roe_bit   (1 << 3)

// SPI Control Register Bits
#define ieop_bit   (1 << 9)
#define	ie_bit     (1 << 8)
#define	irrdy_bit  (1 << 7)
#define	itrdy_bit  (1 << 6)
#define	itoe_bit   (1 << 4)
#define	iroe_bit   (1 << 3)




#define MAXPOLL 100



// IA means 'interaction'
#define USERSPACEIA
//#define DEBUGINFO
//#define DEBUG
//#define DEBUGLL
//#define DEBUGPOLL
//#define ADC_I2C_ADDRESS 0x00
#define ADC_I2C_ADDRESS 0x37
//#define ADC_I2C_ADDRESS 0x3f
#define ADCMAJO 120
#define ADCNAME "adc"
#define ADC_SIZE na_spi_to_adc_size
static u32* adc_physaddr  = ((u32 *) na_spi_to_adc_base);
static u32* adc_virtbase;
static int majornum;
static int opened;

// Now devfs

#ifdef CONFIG_DEVFS_FS
#include <linux/devfs_fs_kernel.h>
static devfs_handle_t adc_devfs_handle = NULL;
const char devfsname[]=ADCNAME;
#endif

MODULE_AUTHOR("TK");
MODULE_DESCRIPTION("adc-module");
MODULE_LICENSE("GPL");

// FUNCTIONS:

static int adc_read(struct file *filp, char *buf, size_t count, loff_t *unused_loff_t);
static int adc_write (struct file *filp, const char *buf, size_t count, loff_t *unused_loff_t);
static int adc_open (struct inode *inode, struct file *filp);
static int adc_release (struct inode *inode, struct file *filp);

static int adc_init ();
static int adc_basic_read (int reg);
static void adc_basic_write (int reg,int value);
static int adc_waitRX();
static int adc_read_cycle(int reg, int count);
static int adc_write_cycle(int reg, int value, int count);
static void adc_reset();
static struct file_operations adc_fops = {
  owner   :   THIS_MODULE,
  read    :    adc_read,
  write   :   adc_write,
  open    :    adc_open,
  release : adc_release,
};


/*
 *   Basic IO Functions
 */


static int adc_basic_read (int reg) {
#ifdef DEBUGLL
  u32 temp;
  printk("Reading from %p",(adc_virtbase + reg));
  temp = readl(adc_virtbase + reg);
  printk(": 0x%08X\n",temp);
  return temp;
#else
  return(readl(adc_virtbase + reg));
#endif
}

static void
adc_basic_write (int reg,int value) {
#ifdef DEBUGLL
  printk("Writing to %p: 0x%08X\n",(adc_virtbase + reg),value);
#endif
  writel(value, adc_virtbase + reg);
}

/**************************************************************
 **************************************************************
 *
 * Reset : 
 * "In situations where the interface sequence is lost, a
 *  write operation of at least 32 serial clock cycles with 
 * DIN high returns the AD7708/AD7718 to this default state 
 * by resetting the part.
 */
static void adc_reset() {
  int testreset=0;
  int compare=0;
  testreset=adc_read_cycle(adc_filter,1);
#ifdef DEBUG
  printk("Testing reset: read filter: 0x%02X. Write: 0x%02X\n",testreset,(~testreset));
#endif
  testreset = ~testreset;
  adc_write_cycle(adc_filter,testreset,1);
  // reset:
  adc_write_cycle(adc_com,0xFFFFFFFF,4);
  mdelay(400); // worst case: oscillator restart + 100ms
  compare=adc_read_cycle(adc_filter,1);
#ifdef DEBUG
  printk("Testing reset: read filter after reset: 0x%02X \n",compare);
#endif
  if ( compare != 0x45) {
    printk(KERN_ERR "Reset failed...gain should be 0x45, but is 0x%02X\n",compare);
  } else {
    printk("ADC: Reset device\n");
  }

}
/**************************************************************
 **************************************************************
 *  Register Access 8/16/24/(32) Bits
 *  count = number of chars to read (max 4)
 */
static int
adc_read_cycle(int reg, int count) {
  int temp=0;
  int i;
  if ( (count > 4) || (count < 0 ) ) {
    printk(KERN_ERR "Invalid number of reads");
    return -EINVAL;
  }
  // first write to comm reg the register address

  if (OC_ISCLEAR(adc_basic_read(spistatus), trdy_bit)) {
    printk("%s: tx not ready\n",__FUNCTION__ );
    return -1;
  }
  // read out any garbage out of rxdata
  temp=adc_basic_read(spirxdata);
#ifdef DEBUGLL
  printk("garbage read: 0x%02X\n",temp);
#endif

  // write to communication register WEN=0 , RWn = 1
  adc_basic_write(spitxdata, ((reg & 0xF) | adc_com_RWn));
 if (adc_waitRX() ) return -1;
  // wait for trdy to come back
#ifdef DEBUG
  printk("write to comreg\n");
#endif
 
  // read out any garbage out of rxdata
  temp=adc_basic_read(spirxdata);
#ifdef DEBUGLL
  printk("garbage read: 0x%02X\n",temp);
#endif

  
  // start count succesive reads:
  temp=0;
  for (i=0; i<count; i++) {
    // write dummy data
    adc_basic_write(spitxdata, 0x80);
    // wait for rrdy to come back
#ifdef DEBUG
    printk("dummy write\n");
#endif
    if (adc_waitRX() ) return -1;
    temp = (temp << 8) | (adc_basic_read(spirxdata) & 0xFF);
#ifdef DEBUG
    printk("read #%d: temp = 0x%08X\n",i,temp);
#endif
  }
  return(temp); // hope we never have to read 32 bit! (signed)
}

/**************************************************************
 **************************************************************
 *  Register Access 8/16/24/(32) Bits
 *  count = number of chars to read (max 4)
 */
static int adc_write_cycle(int reg, int value, int count) {
  int temp=0;
  int i;
  u32 val=(u32) value;
  if ( (count > 4) || (count < 0 ) ) {
    printk(KERN_ERR "Invalid number of reads");
    return -EINVAL;
  }
  // first write to comm reg the register address

  if (OC_ISCLEAR(adc_basic_read(spistatus), trdy_bit)) {
    printk("%s: tx not ready\n",__FUNCTION__ );
    return -1;
  }
  // read out any garbage out of rxdata
  temp=adc_basic_read(spirxdata);
#ifdef DEBUGLL
  printk("garbage read: 0x%02X\n",temp);
#endif

  // write to communication register WEN=0 , RWn = 1
  adc_basic_write(spitxdata, ((reg & 0xF) ));
 if (adc_waitRX() ) return -1;
  // wait for trdy to come back
#ifdef DEBUG
  printk("write to comreg\n");
#endif
 
  // read out any garbage out of rxdata
  temp=adc_basic_read(spirxdata);
#ifdef DEBUGLL
  printk("garbage read: 0x%02X\n",temp);
#endif

  
  // start count succesive writes:
  temp=0;
  for (i=0; i<count; i++) {
    // write data
    temp = ((val >> ((count-i-1)*8)) & 0xff);
    adc_basic_write(spitxdata, temp);
    // wait for rrdy to come back
#ifdef DEBUG
    printk("wrote 0x%02X\n",temp);
#endif
    if (adc_waitRX() ) return -1;
    temp = adc_basic_read(spirxdata);
  }
  return 0;
}


/*
 *   READ
 *   8 Bit: buf[0] must be preloaded with register address, value in buf[1]
 *  16 Bit: buf[0] must be preloaded with register address, value in buf[1] and buf[2]
 */

static int adc_read(struct file *filp, char *buf, size_t count, loff_t *unused_loff_t) {

  int reg=0;
  int value=0;
  //  int minor = MINOR(filp->f_dentry->d_inode->i_rdev);
  if ( !count ) return -EINVAL;
  // take address
  copy_from_user((unsigned char *) &reg, buf,1);
  switch(count) {
  case 2:  // 8 Bit read
    value=adc_read_cycle(reg,1);
    copy_to_user(buf+1, (unsigned char *) &value,1);
    break;
  case 3: // 16 Bit read
    value=adc_read_cycle(reg,2);
    copy_to_user(buf+1, (unsigned char *) &value,2);
    break;
  default:
    return -EINVAL;
    break;
  }
#ifdef DEBUG
  printk("adc read %d Bytes (0x%4X) from register %d\n",count-1,value,reg);
#endif
  return count;
}

/*
 *   WRITE
 *   8 Bit: buf[0] must be preloaded with register address, value in buf[1]
 *  16 Bit: buf[0] must be preloaded with register address, value in buf[1] and buf[2]
 */

static int adc_write (struct file *filp, const char *buf, size_t count, loff_t *unused_loff_t) {
  int reg=0;
  int value=0;
  //  int minor = MINOR(filp->f_dentry->d_inode->i_rdev);
  if ( !count ) return -EINVAL;
  // take address
  copy_from_user((unsigned char *) &reg, buf,1);
  if (reg==adc_rst) {
    adc_reset();
    return count;
  }
  switch(count) {
  case 2:  // 8 Bit write
    copy_from_user((unsigned char *) &value, buf+1 ,1);
    if (adc_write_cycle(reg,value,1)) return -EINVAL;
    break;
  case 3: // 16 Bit read
    copy_from_user((unsigned char *) &value, buf+1 ,2);
    if (adc_write_cycle(reg,value,2)) return -EINVAL;
    break;
  default:
    return -EINVAL;
    break;
  }
#ifdef DEBUG
  printk("i2c write %d Bytes (0x%4X) to register %d\n",count-1,value,reg);
#endif
  return count;
}

/*
 *   OPEN
 */

static int adc_open (struct inode *inode, struct file *filp) {

  int minor = MINOR(inode->i_rdev);
  
  if (minor != 0) { // todo
    printk("only low level register access on minor 0 allowed\n");
    return -ENODEV;
  }
  if (opened) return -EBUSY;
#ifdef DEBUG
  printk("adc open minor #%d\n",minor);
#endif
  MOD_INC_USE_COUNT;
  opened=1;
  return 0;
}

/*
 *   CLOSE/RELEASE
 */

static int adc_release (struct inode *inode, struct file *filp) {

#ifdef DEBUG
  int minor = MINOR(inode->i_rdev);
  printk(KERN_INFO "adc minor #%d released\n",minor);
#endif
  MOD_DEC_USE_COUNT;
  opened=0;
  return 0;
}
#ifdef DEBUGINFO
static void adc_dumpregs() {
  int gain,offset,status,mode,ctrl,filter,ioctrl,id;
  int data;
  gain   = adc_read_cycle(adc_gain,2);
  offset = adc_read_cycle(adc_offset,2);
  status = adc_read_cycle(adc_status,1);
  mode   = adc_read_cycle(adc_mode,1);
  ctrl   = adc_read_cycle(adc_ctrl,1);
  filter = adc_read_cycle(adc_filter,1);
  ioctrl = adc_read_cycle(adc_ioctrl,1);
  data   = adc_read_cycle(adc_data,2);
  id = adc_read_cycle(adc_id,1);
  printk("gain   : 0x%04X \n", gain);
  printk("offset : 0x%04X \n", offset);
  printk("status : 0x  %02X \n", status);
  printk("mode   : 0x  %02X \n", mode);
  printk("ctrl   : 0x  %02X \n", ctrl);
  printk("filter : 0x  %02X \n", filter);
  printk("ioctrl : 0x  %02X \n", ioctrl);
  printk("id     : 0x  %02X \n", id);
  printk("data   : 0x%04X \n", data);
}
#endif

////////////////////////////////////////////////////////////////////////////////
// initialise Opencores i2c ctrl
static int adc_init () {
  int id;
  int result;
  // tie CSn down 
#ifdef CONFIG_DEVFS_FS
#ifdef DEBUGINFO
  printk("Registering DEVFS\n");
#endif
  adc_devfs_handle = devfs_register(NULL,
				    devfsname,
				    //      				    DEVFS_FL_AUTO_DEVNUM,
				    DEVFS_FL_DEFAULT,
				    ADCMAJO,
				    0,
				    S_IFCHR | S_IRUGO | S_IWUGO,
				    &adc_fops,
				    NULL );

  result = devfs_register_chrdev(ADCMAJO, ADCNAME, &adc_fops);
#else
  result = register_chrdev(ADCMAJO, ADCNAME, &adc_fops);
#endif
  if (result < 0) {
    printk(KERN_ERR "Can not get major number %d\n",ADCMAJO);
    return result;
  } else {
    result=ADCMAJO;
    printk(KERN_INFO "Major number of Module: %d\n",result);
  }
  majornum=result;
  adc_virtbase  = (u32*) ioremap_nocache((u32)adc_physaddr,ADC_SIZE);
#ifdef DEBUGINFO
  printk(KERN_INFO "Remapped from %p to %p\n",adc_physaddr, adc_virtbase);
#endif

  adc_basic_write(spislaveselect, 1);
  adc_reset();
  opened=0;
  printk(KERN_INFO "ADC: checking ID Register: ");
  id = adc_read_cycle(adc_id,1);
  printk("0x%02x = ",id);
  if ( (id >> 4) == 4 ) 
    printk ("AD7718, Rev %d - OK\n",id & 0xF);
  else 
    if ( (id >> 4) == 5 ) 
      printk ("AD7708, Rev. %d - OK\n",id & 0xF);
    else {
      printk ("unknown\n");
      printk (KERN_ERR "WARNING: unknown id read for ADC!\n");
    }
  


#ifdef DEBUGINFO
  adc_dumpregs();
#endif
  return 0;
  // do Fig. 16 and 17 in Manual

}

////////////////////////////////////////////////////////////////////////////////
// Poll for trdy and rrdy

static int adc_waitRX() {
  int i=0;
  while (OC_ISCLEAR(adc_basic_read(spistatus), rrdy_bit))  {
    i++;
    if (i == MAXPOLL) {
      printk("%s: timeout !!! after %d cycles\n",__FUNCTION__,MAXPOLL);
      return -1;
    }
  }  
#ifdef DEBUGPOLL
  printk("%s: wait for RRDY: Waited %d loops\n",__FUNCTION__,i);
#endif
  return 0;
}
////////////////////////////////
#if 0
static int adc_waitTX() {
  int i=0;
  while (OC_ISCLEAR(adc_basic_read(spistatus), trdy_bit))  {
    i++;
    if (i == MAXPOLL) {
      printk("%s: timeout !!! after %d cycles\n",__FUNCTION__ ,MAXPOLL);
      return -1;
    }
  }
#ifdef DEBUGPOLL
  printk("%s: wait for TRDY: Waited %d loops\n",__FUNCTION__ ,i);
#endif
  return 0;
}      
#endif


/******************************************************************************/
// Module Stuff
/******************************************************************************/

#define MAXI 3
int init_module(void) {
  printk(KERN_WARNING "adc module init (compiled "__DATE__", "__TIME__")\n");



/*   printk(KERN_WARNING "ADC: Module init (compiled "__DATE__", "__TIME__")\n"); */
/* #ifdef CONFIG_DEVFS_FS */
/*   result = devfs_register_chrdev(ADCMAJO, ADCNAME, &adc_fops); */
/* #else */
/*   result = register_chrdev(ADCMAJO, ADCNAME, &adc_fops); */
/* #endif */
/*   if (result < 0) { */
/*     printk(KERN_ERR "Can not get major number %d\n",ADCMAJO); */
/*     return result; */
/*   } else { */
/* #ifdef DEBUGINFO */
/*     printk(KERN_INFO "Major number of Module: %d\n",result); */
/* #endif */
/*   } */
/*   majornum=result; */

/*   adc_virtbase  = (u32*) ioremap_nocache((u32)adc_physaddr,ADC_SIZE); */
/* #ifdef DEBUGINFO */
/*   printk(KERN_INFO "Remapped from %p to %p\n",adc_physaddr, adc_virtbase); */
/* #endif */

  return(adc_init());




  return 0;
}

void cleanup_module(void) { 
  devfs_unregister( adc_devfs_handle );
  adc_basic_write(spislaveselect, 0);
  printk(KERN_WARNING "adc module exit\n");
  iounmap((void *)adc_virtbase);
  unregister_chrdev( majornum , ADCNAME);
}
